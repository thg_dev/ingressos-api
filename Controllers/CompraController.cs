using crud_api.Domain;
using Microsoft.AspNetCore.Mvc;

namespace crud_api.Controllers;

[ApiController]
[Route("[controller]")]
public class CompraController : ControllerBase
{

    [HttpPost("AdicionarCompra")]
    public IActionResult PostAdicionarCompra(Compra Compra)
    {
        BancoDeDados._compra.Add(Compra);
        return Ok(Compra);

        
    }

    [HttpGet("ObterCompra/{ingresso}")]
    public IActionResult GetObterCompra(string ingresso)
    {
        Compra compraEncontrada = null;
        foreach (var compra in BancoDeDados._compra)
        {
            if (compra.Ingresso == ingresso)
            {
                compraEncontrada = compra;
            }
        }
    
        return Ok(compraEncontrada);
    }

    [HttpDelete("RemoverCompra/{ingresso}")]
    public IActionResult DeleteRemoverCompra(string ingresso)
    {
        Compra compraEncontrada = null;
        foreach (var compra in BancoDeDados._compra)
        {
            if (compra.Ingresso == ingresso)
            {
                compraEncontrada = compra;
            }
        }
        BancoDeDados._compra.Remove(compraEncontrada);
        return Ok(BancoDeDados._compra);
    }






}
