using crud_api.Domain;
using Microsoft.AspNetCore.Mvc;

namespace crud_api.Controllers;

[ApiController]
[Route("[controller]")]
public class IngressoController : ControllerBase
{
    private readonly ILogger<IngressoController> _logger;

    public IngressoController(ILogger<IngressoController> logger)
    {
        _logger = logger;
    }

    [HttpPost("AdicionarIngresso")]
    public IActionResult PostAdicionarIngresso(Ingresso ingresso)
    {
        if (ingresso.Cadeira == "xd")
            return NotFound("Erro");

        BancoDeDados._ingressos.Add(ingresso);
        return Ok(ingresso);
    }

    [HttpGet("ObterIngressos")]
    public IActionResult GetObterIngressos()
    {
        return Ok(BancoDeDados._ingressos);
    }
}
