using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace crud_api.Domain
{
    public class Compra
    {
        public string Cliente  { get; set; }   
        public string Ingresso { get; set; }
        public int Quantidade { get; set; }
        public double Preco { get; set; }
    }
}