using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace crud_api.Domain
{
    public class Ingresso
    {
        public string Jogo { get; set; }   
        public string Cadeira { get; set; }
        public string Bloco { get; set; }
    }
}