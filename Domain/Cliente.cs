using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace crud_api.Domain
{
    public class Cliente
    {
        public string NomeCompleto { get; set; }   
        public DateTime DataNascimento { get; set; }
        public int Cpf { get; set; }
        public string Email { get; set; }
    }
}